//
//  App.swift
//  NaRogah
//
//  Created by User on 19/01/2019.
//  Copyright © 2019 Александр Чернышов. All rights reserved.
//

import Foundation

var app = Model()

func getFlag() -> Bool {
    return app.flag
}

func setFlag(flag: Bool) {
    app.flag = flag
}

func getRequest() {
    guard let url = URL(string: "https://jsonplaceholder.typicode.com/posts") else {return}
    
    let session = URLSession.shared
    session.dataTask(with: url) { (data, response, error) in
        if let response = response {
            print(response)
        }
        
        guard let data = data else {return}
        print(data)
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            print(json)
        } catch {
            print(error)
        }
        
    }.resume()
}

func reg(email: String, pass: String) {
    app.email = email
    app.pass = pass
}

func login(email: String, pass: String) {
    if email == app.email && pass == app.pass {
        setFlag(flag: true)
    }
}
