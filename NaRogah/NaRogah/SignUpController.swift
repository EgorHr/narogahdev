//
//  SignUpController.swift
//  NaRogah
//
//  Created by User on 18/01/2019.
//  Copyright © 2019 Александр Чернышов. All rights reserved.
//

import UIKit

class SignUpController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passField: UITextField!
    @IBOutlet weak var repeatPassField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailField.delegate = self
        passField.delegate = self
        repeatPassField.delegate = self
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with: UIEvent?){
        self.view.endEditing(true)
    }
    
    func goToMainScreen(){
        let vc = storyboard?.instantiateViewController(withIdentifier: "MainScreen")
        navigationController?.pushViewController(vc!, animated: true)
    }
    
    func signUp(email: String, pass: String){
        /* Отправка запроса на сервер */
        reg(email: email, pass: pass)
        
        goToMainScreen() /* Если вошли в аккаунт, то переходим на страницу с меню */
    }
    
    @IBAction func onButtonTouch(_ sender: UIButton) {
        view.endEditing(true)
        
        let email = emailField.text!
        let pass = passField.text!
        let r_pass = repeatPassField.text!
        
        emailField.text = ""
        passField.text = ""
        repeatPassField.text = ""
        
        if email != "" && pass != "" && r_pass != ""{
            if pass == r_pass {
                signUp(email: email, pass: pass)
            }
        }
    }
}
