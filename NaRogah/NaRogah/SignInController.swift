//
//  SignInController.swift
//  NaRogah
//
//  Created by User on 17/01/2019.
//  Copyright © 2019 Александр Чернышов. All rights reserved.
//

import UIKit

class SignInController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailField.delegate = self
        passField.delegate = self
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with: UIEvent?){
        self.view.endEditing(true)
    }
    
    func goToMainScreen(){
        let vc = storyboard?.instantiateViewController(withIdentifier: "MainScreen")
        navigationController?.pushViewController(vc!, animated: true)
    }
    
    func signIn(email: String, pass: String){
        /* Отправка запроса на сервер */
        //getRequest()
        
        //setFlag(flag: true)
        login(email: email, pass: pass)
        if getFlag() {
            goToMainScreen()
        }
    }
    

    @IBAction func onButtonTouch(_ sender: UIButton) {
        view.endEditing(true)
        
        let email = emailField.text!
        let pass = passField.text!
        
        emailField.text = ""
        passField.text = ""
        
        if email != "" && pass != ""{
            signIn(email: email, pass: pass)
        }
    }
    @IBAction func goToSignUp(_ sender: UIButton) {
        view.endEditing(true)
    }
}
