//
//  ViewController.swift
//  NaRogah
//
//  Created by Александр Чернышов on 17/01/2019.
//  Copyright © 2019 Александр Чернышов. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //@IBOutlet weak var barAuthBtn: UIBarButtonItem!
    @IBOutlet weak var barAuthBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if getFlag() {
            barAuthBtn.setTitle("Выйти", for: .normal)
        } else {
            barAuthBtn.setTitle("Авторизация", for: .normal)
        }
    }

    @IBAction func authBtnTouch(_ sender: Any) {
        if getFlag() {
            setFlag(flag: false)
            barAuthBtn.setTitle("Авторизация", for: .normal)
        }
    }
}

